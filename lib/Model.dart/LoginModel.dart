

import 'dart:convert';



LoginModel loginModelFromJson(String str) =>
    LoginModel.fromJson(json.decode(str));

String loginModelToJson(LoginModel data) => json.encode(data.toJson());

class LoginModel {
  LoginModel({
    required this.token,
    required this.expiresIn,
    required this.profile,
  });

  String token;
  int expiresIn;
  Profile profile;

  factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
        token: json["token"],
        expiresIn: json["expires_in"],
        profile: Profile.fromJson(json["profile"]),
      );

  Map<String, dynamic> toJson() => {
        "token": token,
        "expires_in": expiresIn,
        "profile": profile.toJson(),
      };
}

class Profile {
  Profile({
    required this.userId,
    required this.userUid,
    required this.userName,
    required this.passwordHash,
    required this.userLanguage,
    required this.userCategory,
    required this.email,
    required this.isOnline,
    this.menuRights,
    this.userRole,
    this.companyInfo,
    this.branchInfo,
  });

  String userId;
  String userUid;
  String userName;
  String passwordHash;
  String userLanguage;
  String userCategory;     
  String email;
  int isOnline;
  dynamic menuRights;          
  dynamic userRole;
  dynamic companyInfo;
  dynamic branchInfo;

  factory Profile.fromJson(Map<String, dynamic> json) => Profile(
        userId: json["UserID"],
        userUid: json["UserUID"],
        userName: json["UserName"],
        passwordHash: json["PasswordHash"],
        userLanguage: json["UserLanguage"],
        userCategory: json["UserCategory"],
        email: json["Email"],
        isOnline: json["IsOnline"],
        menuRights: json["menuRights"],
        userRole: json["UserRole"],
        companyInfo: json["CompanyInfo"],
        branchInfo: json["BranchInfo"],
      );

  Map<String, dynamic> toJson() => {
        "UserID": userId,
        "UserUID": userUid,
        "UserName": userName,
        "PasswordHash": passwordHash,
        "UserLanguage": userLanguage,
        "UserCategory": userCategory,
        "Email": email,
        "IsOnline": isOnline,
        "menuRights": menuRights,
        "UserRole": userRole,
        "CompanyInfo": companyInfo,
        "BranchInfo": branchInfo,
      };
}

// class LoginRequestModel extends StatelessWidget {

//   String username;
//  String pasword;
//   const LoginRequestModel({
//     required this.username,
//     required this.password,
//   });

//   @override

//   Widget build(BuildContext context) {
//     return const Placeholder();
//   }
// }

// class loginResponseModel {
//   final String token;
//   final String error;

//   loginResponseModel({required this.token, required this.error});

//   factory loginResponseModel.fromJson(Map<String, dynamic> json) {
//     return loginResponseModel(
//       token: json["token"] != null ? json["token"] : "",
//       error: json["error"] != null ? json["error"] : "",
//     );
//   }
// }

// class LoginRequestModel {
//   String email;
//   String password;

//   LoginRequestModel({
//     required this.email,
//     required this.password,
//   });

//   Map<String, dynamic> toJson() {
//     Map<String, dynamic> map = {
//       'email': email.trim(),
//       'password': password.trim(),
//     };
//     return map;
//   }
// }


// success 200, 201 rw error 400, 401, 404
// 500 server issue
// display snackbar in login successful plus failure ko pani
// get = no body mstly
// 


