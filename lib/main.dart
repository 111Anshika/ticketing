import 'package:flutter/material.dart';

import 'screens/login_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Ticketing Login',
      theme: ThemeData(
        
        primarySwatch: Colors.red,
      ),
      home: Login_screen(),
      // home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

