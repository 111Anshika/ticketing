import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:tickets/Model.dart/LoginModel.dart';
import 'package:tickets/Services/Login_Services.dart';
import 'package:tickets/screens/Base_page.dart';
import 'package:tickets/screens/home_screen.dart';
import 'package:tickets/screens/registration_screen.dart';

TextEditingController emailController = TextEditingController();
TextEditingController passwordController = TextEditingController();

class Login_screen extends StatefulWidget {
  const Login_screen({super.key});

  @override
  State<Login_screen> createState() => _Login_screenState();
}

class _Login_screenState extends State<Login_screen> {
  //form key
  final _formKey = GlobalKey<FormState>();
// LoginModel Model;

  // editing controller

  var emailField = TextFormField(
    autofocus: false,
    controller: emailController,
    keyboardType: TextInputType.emailAddress,
    //
    validator: (input) =>
        !input!.contains("@") ? "Email ID sould be valid" : null,
    onSaved: (value) {
      emailController.text = value!;
    },
    textInputAction: TextInputAction.next,
    decoration: InputDecoration(
      prefixIcon: Icon(Icons.mail),
      contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
      hintText: "Email",
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
      ),
    ),
  );

  //password field
  final passwordField = TextFormField(
    autofocus: false,
    controller: passwordController,
    // obscureText: true,

    // keyboardType: TextInputType.emailAddress,

    validator: (input) =>
        input!.length < 3 ? "Password should be more than 3 characters" : null,
    onSaved: (value) {
      emailController.text = value!;
    },
    textInputAction: TextInputAction.done,
    decoration: InputDecoration(
      prefixIcon: Icon(Icons.vpn_key),
      contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
      hintText: "Password",
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
      ),
    ),
  );
  // );

  Widget loginButton() => Material(
        elevation: 5,
        borderRadius: BorderRadius.circular(30),
        color: Colors.redAccent,
        child: MaterialButton(
          padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          minWidth: MediaQuery.of(context).size.width,
          onPressed: () async {
            if (_formKey.currentState!.validate()) {
              print(emailController.text);
              print(passwordController.text);

              LoginServices apiService = LoginServices();
              apiService
                  .login(emailController.text, passwordController.text)
                  .then((value) {
                print(value);

                if(value?.token != null) {

                

                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text('Logged in successfully'),
                    backgroundColor: Colors.green,
                  ),
                );

                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Home_screen()));
              }
              else{
                
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text('Error in Logging'),
                    backgroundColor: Colors.green,
                  ),
                );
              }
            });

              // await LoginServices()
              //     .login(emailController.text, passwordController.text, context);
              //      .then((value) {
              //      if (value ?. token.toString() != null){

              //     ScaffoldMessenger.of(context).showSnackBar(
              //   const SnackBar(
              //     content: Text('Logged in successfully'),
              //     backgroundColor: Colors.green,
              //   ),

              // );
              // Navigator.push(context, MaterialPageRoute(builder: (context)=> Home_screen()));
              //      }
              //      else{
              //       ScaffoldMessenger.of(context).showSnackBar(
              //   const SnackBar(
              //     content: Text('Log in unsuccessfull'),
              //     backgroundColor: Colors.red,
              //   ),
              //       );
              //      }
              //     });
            }
          },
          child: Text(
            'login',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    //email field

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: 45),
                    emailField,
                    SizedBox(height: 25),
                    passwordField,
                    SizedBox(height: 35),
                    loginButton(),
                    SizedBox(height: 15),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("Don't have an account?"),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        Registration_screen()));
                          },
                          child: Text(
                            "SignUp",
                            style: TextStyle(
                                color: Colors.redAccent,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
