// import 'package:a2zticketing/pages.dart/eventPage.dart';
import 'package:flutter/material.dart';
// import 'package:flutter/src/widgets/framework.dart';
// import 'package:flutter/src/widgets/placeholder.dart';
import 'home_screen.dart';

class basePage extends StatefulWidget {
  const basePage({super.key});

  @override
  State<basePage> createState() => _basePageState();
}

class _basePageState extends State<basePage> {

  static const List<Widget> Pages =[
    // Home_screen(),
        Text('event Page', style: TextStyle(fontSize: 35,fontWeight: FontWeight.bold),),

    Text('Profile Page', style: TextStyle(fontSize: 35,fontWeight: FontWeight.bold),),
    Text('Notification Page', style: TextStyle(fontSize: 35,fontWeight: FontWeight.bold),),
    Text('Settings Page', style: TextStyle(fontSize: 35,fontWeight: FontWeight.bold),),

  ];

  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.indigo.shade900,

        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          onPressed: (){},
          child: Icon(Icons.qr_code_scanner_rounded),
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
          
          ),
          

          // bottomNavigationBar:BottomAppBar(
            
          //   notchMargin: 5.0,
          //   shape: CircularNotchedRectangle(),
          //   color: Colors.black,
          //     child: Padding(
          //       padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
          //       child: Row(
          //           mainAxisAlignment: MainAxisAlignment.spaceAround,
          //         children: [
          //           Padding(
          //             padding: EdgeInsets.only(left: 10.0),
          //             child: Column(
          //               mainAxisSize: MainAxisSize.min,
          //               children: [
          //                  Icon(Icons.event,color: Colors.white),
          //                   Text("Events", style: TextStyle(color: Colors.white),),
          //               ],
          //             ),
          //             ),
              
          //              Padding(
          //             padding: EdgeInsets.only(left: 10.0),
          //             child: Column(
          //               mainAxisSize: MainAxisSize.min,
          //               children: [
          //                  Icon(Icons.person_2,color: Colors.white),
          //                   Text("Profile", style: TextStyle(color: Colors.white),),
          //               ],
          //             ),
          //             ),
          //              Padding(
          //             padding: EdgeInsets.only(left: 10.0),
          //             child: Column(
          //               mainAxisSize: MainAxisSize.min,
          //               children: [
          //                  Icon(Icons.settings,color: Colors.white),
          //                   Text("Settings", style: TextStyle(color: Colors.white),),
          //               ],
          //             ),
          //             ),
                   
          //             Padding(
          //             padding: EdgeInsets.only(left: 10.0),
          //             child: Column(
          //               mainAxisSize: MainAxisSize.min,
          //               children: [
          //                  Icon(Icons.notifications,color: Colors.white),
          //                   Text("Notification", style: TextStyle(color: Colors.white),),
          //               ],
          //             ),
          //             ),

          //         ],
          //       ),
          //     ),
          // ),

        bottomNavigationBar:
        BottomNavigationBar(
          currentIndex: selectedIndex,
          onTap: (value) {
            setState(() {
              selectedIndex =value;
            });
            print(selectedIndex);
          },
            
          backgroundColor: Colors.white,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.event,color: Colors.black),
              label: "Events",
              ),

               BottomNavigationBarItem(
              icon: Icon(Icons.person_2, color: Colors.black),
              label: "Profile",
              ),

               BottomNavigationBarItem(
              icon: Icon(Icons.notifications, color: Colors.black),
              label: "Notification",
              ),

               BottomNavigationBarItem(
              icon: Icon(Icons.settings, color: Colors.black),
              label: "Settings",
              ),

          ],
          ) ,

        body:
           Pages[selectedIndex]
          );
        // ),
    // );
  }
}